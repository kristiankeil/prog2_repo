//
// Created by Kristian Keil on 05.04.21.
//

#import <iostream>
#import "cFunkmast.h"

using namespace std;

int main(){
    cFunkmast funkmasten[100];

    for(int i=0; i < 5; i++){
        funkmasten[i].eingabe();
    }

    for(int i=0; i < 5; i++){
        funkmasten[i].ausgabe();
    }

    return 0;
}

