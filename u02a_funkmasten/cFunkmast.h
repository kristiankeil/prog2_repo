//
// Created by Kristian Keil on 05.04.21.
//

#ifndef PROG2_REPO_CFUNKMAST_H
#define PROG2_REPO_CFUNKMAST_H


class cFunkmast {
private:
    int anz_antennen;
    double reichweite;
    double hoehe;
    double geo_breite;
    double geo_hoehe;

public:
    cFunkmast(int anz_antennen_in = 0, double reichweite_in = 0.0, double hoehe_in = 0.0, double geo_breite_in = 0.0,
              double geo_hoehe_in = 0.0);
    void eingabe();
    void ausgabe();
};


#endif //PROG2_REPO_CFUNKMAST_H
