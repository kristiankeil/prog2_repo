//
// Created by Kristian Keil on 05.04.21.
//

#include "cFunkmast.h"
#include <iostream>

using namespace std;

cFunkmast::cFunkmast(int anz_antennen_in, double reichweite_in, double hoehe_in, double geo_breite_in,
                     double geo_hoehe_in) {
    anz_antennen = anz_antennen_in;
    reichweite = reichweite_in;
    hoehe = hoehe_in;
    geo_breite = geo_breite_in;
    geo_hoehe = geo_hoehe_in;
}

void cFunkmast::ausgabe() {
    if(anz_antennen > 0){
        cout << anz_antennen << "\t" << reichweite << "\t" << hoehe << "\t" << geo_breite << "\t" << geo_hoehe << endl;
    }
}

void cFunkmast::eingabe() {
    cout << "Anzahl Antennen: ";
    cin >> anz_antennen;
    cout << "Reichweite: ";
    cin >> reichweite;
    cout << "Hoehe: ";
    cin >> hoehe;
    cout << "Geografische Breite: ";
    cin >> geo_breite;
    cout << "Geografische Hoehe: ";
    cin >> geo_hoehe;
}
